import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InputNumberModule } from 'primeng/inputnumber';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { MdbAccordionModule } from 'mdb-angular-ui-kit/accordion';
import { MdbCarouselModule } from 'mdb-angular-ui-kit/carousel';
import { MdbCheckboxModule } from 'mdb-angular-ui-kit/checkbox';
import { MdbCollapseModule } from 'mdb-angular-ui-kit/collapse';
import { MdbDropdownModule } from 'mdb-angular-ui-kit/dropdown';
import { MdbFormsModule } from 'mdb-angular-ui-kit/forms';
import { MdbModalModule } from 'mdb-angular-ui-kit/modal';
import { MdbPopoverModule } from 'mdb-angular-ui-kit/popover';
import { MdbRadioModule } from 'mdb-angular-ui-kit/radio';
import { MdbRangeModule } from 'mdb-angular-ui-kit/range';
import { MdbRippleModule } from 'mdb-angular-ui-kit/ripple';
import { MdbScrollspyModule } from 'mdb-angular-ui-kit/scrollspy';
import { MdbTabsModule } from 'mdb-angular-ui-kit/tabs';
import { MdbTooltipModule } from 'mdb-angular-ui-kit/tooltip';
import { MdbValidationModule } from 'mdb-angular-ui-kit/validation';
import { ButtonsMdbComponent } from './buttons-mdb/buttons-mdb.component';
import { MainComponent } from './main/main.component';
import { CarouselComponent } from './carousel/carousel.component';
import { FooterComponent } from './footer/footer.component';
import { ChildComponentComponent } from './child-component/child-component.component';
import { ChildComponent2Component } from './child-component2/child-component2.component';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { PersonComponent } from './person/person.component';
import { SumComponent } from './sum/sum.component';
import { UrlsComponent } from './urls/urls.component';
import { BooleansComponent } from './booleans/booleans.component';
import { ColorPickerModule } from 'primeng/colorpicker';
import { GreetingsPipe } from './Pipes/greatings.pipes';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import { ScrollComponent } from './scroll/scroll.component';
import { FetcherComponent } from './fetcher/fetcher.component';
import { QuizComponent } from './quiz/quiz.component';
import { FoxFetcherComponent } from './fox-fetcher/fox-fetcher.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    ButtonsMdbComponent,
    MainComponent,
    CarouselComponent,
    FooterComponent,
    ChildComponentComponent,
    ChildComponent2Component,
    PersonComponent,
    SumComponent,
    UrlsComponent,
    BooleansComponent,
    GreetingsPipe,
    ScrollComponent,
    FetcherComponent,
    QuizComponent,
    FoxFetcherComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MdbAccordionModule,
    MdbCarouselModule,
    MdbCheckboxModule,
    MdbCollapseModule,
    MdbDropdownModule,
    MdbFormsModule,
    MdbModalModule,
    MdbPopoverModule,
    MdbRadioModule,
    MdbRangeModule,
    MdbRippleModule,
    MdbScrollspyModule,
    MdbTabsModule,
    MdbTooltipModule,
    MdbValidationModule,
    CardModule,
    ButtonModule,
    FormsModule,
    InputNumberModule,
    ColorPickerModule,
    ScrollPanelModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
