import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonsMdbComponent } from './buttons-mdb.component';

describe('ButtonsMdbComponent', () => {
  let component: ButtonsMdbComponent;
  let fixture: ComponentFixture<ButtonsMdbComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonsMdbComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonsMdbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
