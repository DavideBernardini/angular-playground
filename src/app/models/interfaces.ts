import { RequestOriginTool, RequestStatus, RequestType } from './pending-request.enum';

export type RequestWithoutNames =
  | RequestBase &
  (
    | {
      requestType: RequestType.REQUEST_MIFID_PERMISSIONS;
      requestDetails: RequestMifidPermissionDetails;
    }
    | {
      requestType: RequestType.REQUEST_UPDATE_UCHEDGE_USERS_PERMISSIONS;
      requestDetails: RequestUpdateUchedgeUsersPermissionsDetails;
    }
    | {
      requestType: RequestType.REQUEST_ADD_USERS_TO_UCHEDGE;
      requestDetails: RequestAddUsersToUchedgeDetails;
    }
    | {
      requestType: RequestType.REQUEST_EBICS_USER_CREATION;
      requestDetails: RequestEbicsUserCreationDetails;
    }
    | {
      requestType: RequestType.REQUEST_ADD_USERS_TO_UCPAYFX;
      requestDetails: RequestAddUsersToUcpayfxDetails;
    }
    | {
      requestType: RequestType.REQUEST_UPDATE_UCPAYFX_USERS_PERMISSIONS;
      requestDetails: RequestUpdateUcpayfxUsersPermissionsDetails;
    }
    | {
      requestType: RequestType.REQUEST_USER_CREATION;
      requestDetails: RequestUserCreationDetails;
    }
    | {
      requestType: RequestType.REQUEST_EBICS_ACCOUNT_CREATION;
      requestDetails: RequestEbicsAccountCreationDetails;
    }
    | {
      requestType: RequestType.REQUEST_USER_NDG_CREATION;
      requestDetails: RequestUserNdgCreationDetails;
    }
    | {
      requestType: RequestType.REQUEST_EBICS_USERS_DELETION;
      requestDetails: RequestEbicsUsersDeletionDetails;
    }
    | {
      requestType: RequestType.REQUEST_EBICS_ACCOUNTS_DELETION;
      requestDetails: RequestEbicsAccountsDeletionDetails;
    }
    | {
      requestType: RequestType.REQUEST_EBICS_CUSTOMER_CREATION;
      requestDetails: RequestEbicsCustomerCreationDetails;
    }
    | {
      requestType: RequestType.REQUEST_POA_PROXY_CREATION;
      requestDetails: RequestPoaProxyCreationDetails;
    }
    | {
      requestType: RequestType.REQUEST_POA_PROXY_NDG_CREATION;
      requestDetails: RequestPoaProxyCreationDetails;
    }
    | {
      requestType: RequestType.REQUEST_POA_PROXY_CREATION_ADMIN;
      requestDetails: RequestPoaProxyCreationDetails;
    }
    | {
      requestType: RequestType.REQUEST_POA_PROXY_NDG_CREATION_ADMIN;
      requestDetails: RequestPoaProxyCreationDetails;
    }
  );

export type Request = RequestWithoutNames & {
  tool: RequestOriginTool;
  initiatorName: string;
  approverName: string;
  approver2Name: string;
};

export interface RequestBase {
  requestId: number;
  requestStatus: RequestStatus;
  createdAt: number;
  initiator: string;
  approver?: string;
  approver2?: string;
}

export interface RequestPoaProxyCreationDetails {
  firstName: string;
  lastName: string;
  birthday: string;
  proxyNdg: string;
  signaturePower: string;
  legalPosition: string;
  debitCard: boolean;
  creditCard: boolean;
  eBanking: boolean;
  accounts: {
    branch: string;
    accountType: string;
    accountNumber: string;
  }[];
}

export interface RequestUserCreationDetails {
  legalSource: string;
  companyName: string;
  productIds: string[];
  firstName: string;
  lastName: string;
  birthday: string;
  tinNumber: string;
}

export interface RequestUserNdgCreationDetails {
  firstName: string;
  lastName: string;
  birthday: string;
}

export interface RequestEbicsUsersDeletionDetails {
  ebicsCustomerId: string;
  ebicsUsers: {
    ebicsUserId: string;
    ndg: string;
    fullName: string;
  }[];
}
export interface RequestEbicsCustomerCreationDetails {
  language: string;
  customer: {
    customerNdg: string;
    companyName: string;
    contactName: string;
    contactEmail: string;
    country: string;
    city: string;
    street: string;
    zipCode: string;
  };
  users: {
    ndg: string;
    firstName: string;
    lastName: string;
    signatureClassCode: string;
  }[];
  orderTypes: {
    upload: string[];
    download: string[];
  };
  account: {
    iban: string;
    bic: string;
    currency: string;
  };
}

export interface RequestEbicsAccountCreationDetails {
  ebicsCustomerId: string;
  account: {
    accountNumber: string;
    bankCode: string;
    iban: string;
    bic: string;
    currency: string;
    zeroStat: boolean;
    direction: string;
    amount: string;
    orderTypes: {
      download: string[];
    };
    users: {
      ebicsUserId: string;
      ndg: string;
      fullName: string;
    }[];
  };
}

export interface RequestEbicsAccountsDeletionDetails {
  ebicsCustomerId: string;
  accounts: {
    typeCode: string;
    accountNumber: string;
    bankCode: string;
    currency: string;
    bic: string;
    iban: string;
  }[];
  customerNdg: string;
  customerName: string;
  initiatorUserId: string;
}

export interface RequestEbicsUserCreationDetails {
  ebicsCustomerId: string;
  fullName: string;
  ndg: string;
  signatureClass: string;
  orderTypes: {
    upload: string[];
    download: string[];
  };
  accounts: {
    accountNumber: string;
    bankCode: string;
    iban: string;
    bic: string;
    currency: string;
  }[];
}

export interface RequestMifidPermissionDetails {
  fullName: string;
  roleName: string;
  userNdg: string;
  globalId: string;
  permissionNames: string[];
}

export interface RequestAddUsersToUchedgeDetails {
  customerNdg: string;
  customerName: string;
  legalSource: string;
  users: {
    fullName: string;
    roleName: string;
    globalId: string;
    permissionNames: string[];
  }[];
}

export interface RequestUpdateUchedgeUsersPermissionsDetails {
  customerNdg: string;
  customerName: string;
  legalSource: string;
  users: {
    fullName: string;
    roleName: string;
    globalId: string;
    permissionNames: string[];
    userNdg: string;
  }[];
}

export interface RequestAddUsersToUcpayfxDetails {
  customerNdg: string;
  customerName: string;
  legalSource: string;
  users: {
    fullName: string;
    roleName: string;
    globalId: string;
    permissionNames: string[];
  }[];
}

export interface RequestUpdateUcpayfxUsersPermissionsDetails {
  customerNdg: string;
  customerName: string;
  legalSource: string;
  users: {
    fullName: string;
    roleName: string;
    globalId: string;
    permissionNames: string[];
  }[];
}

export interface User {
  ndg: string;
  portalId: string;
  firstName: string;
  lastName: string;
}

export interface UserName {
  firstName: string;
  lastName: string;
}

export interface RequestUsers {
  initiatorName: string;
  approverName: string;
  approver2Name: string;
}
