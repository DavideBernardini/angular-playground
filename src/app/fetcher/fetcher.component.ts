import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-fetcher',
    templateUrl: './fetcher.component.html',
    styleUrls: ['./fetcher.component.scss']
})
export class FetcherComponent implements OnInit {

    isContentLoaded: boolean;
    urlImage: string;

    constructor(private httpClient: HttpClient) {
        this.isContentLoaded = false;
        this.urlImage = '';
    }

    ngOnInit(): void {
        this.dataFetcher();
    }

    dataFetcher(): void {
      this.httpClient
        .get('https://dog.ceo/api/breeds/image/random')
        .subscribe((response: any) => {
          this.urlImage = response.message;
          this.isContentLoaded = true;
        });
    }

}
