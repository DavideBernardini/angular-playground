import { Component, EventEmitter, Input, OnInit, Output, QueryList, SimpleChanges, ViewChildren, ElementRef } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { PendingRequestsService } from 'app/@core/services/pending-requests.service';
import { PendingRequestModalComponent } from '../pending-request-modal/pending-request-modal.component';
import { CustomerMicrositesPermissionService } from 'app/@core/services/customer-microsites-permission.service';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import { NavigationStart, Router } from '@angular/router';
import { Subscription, catchError, throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { RequestFilters } from '../requests-filters/requests-filters.component';
import { RequestStatus, RequestType } from 'app/@shared/models/pending-request.enum';
import { Customer } from 'app/@shared/models/customer-microsites';
import { Request } from 'app/@shared/models/pending-request';

@Component({
  selector: 'wkp-pending-requests-table',
  templateUrl: './pending-requests-table.component.html',
  styleUrls: ['./pending-requests-table.component.scss']
})
export class PendingRequestsTablelComponent implements OnInit {
  @Output() ongetRequestsNumber = new EventEmitter<number>();

  selectedCustomer: Customer;

  pendingRequests: Request[] = [];
  filteredRequests: Request[] = [];
  sortedRequests: Request[] = [];

  areRequestsLoaded = false;
  requestType = RequestType;
  requestStatus = RequestStatus;

  sortColumnIndex = -1;
  sortDirection = 1;

  currentPage: number = 1;
  totalPages: number = 0;
  itemsPerPage: number = 9;
  paginationArray: number[] = [];

  requestFilters: RequestFilters = {
    searchName: '',
    statuses: [],
    tool: '',
    startDateStamp: 0,
    endDateStamp: 0
  };

  isInHistoryPage: boolean = false;

  private subscriptions: { [key: string]: Subscription } = {};

  constructor(
    private customerMicrositesPermissionService: CustomerMicrositesPermissionService,
    private pendingRequestsService: PendingRequestsService,
    private modalService: NgbModal,
    private translateService: TranslateService,
    private router: Router,
    private elementRef: ElementRef
  ) { }

  ngOnInit(): void {
    if (this.router.url.includes('/da-tool/pending-requests-history')) {
      this.isInHistoryPage = true;
    }
    this.subscriptions.selectedCustomer = this.customerMicrositesPermissionService.selectedCustomer$.subscribe((customer) => {
      this.selectedCustomer = customer;
      if (this.selectedCustomer) {
        this.subscriptions.requests = this.pendingRequestsService
          .getRequests(this.selectedCustomer.customerNdg, this.isInHistoryPage)
          .pipe(
            catchError((error: HttpErrorResponse) => {
              this.areRequestsLoaded = true;

              return throwError(() => new Error(`Something bad happened: ${error}`));
            })
          )
          .subscribe((requests) => {
            if (this.isInHistoryPage) {
              this.pendingRequests = requests
                .filter((request) => request.requestStatus !== RequestStatus.REQUEST_PENDING)
                .sort((a, b) => b.createdAt - a.createdAt);
            } else {
              this.pendingRequests = requests
                .filter((request) => request.requestStatus === RequestStatus.REQUEST_PENDING)
                .sort((a, b) => b.createdAt - a.createdAt);
            }

            this.filteredRequests = [...this.pendingRequests];
            this.sortedRequests = [...this.pendingRequests];
            this.calculatePagination();

            this.areRequestsLoaded = true;

            this.ongetRequestsNumber.emit(this.pendingRequests.length);
          });
      }
    });
  }

  addRequestDetails(pendingRequest: Request) {
    const datePipe = new DatePipe('en-US');

    switch (pendingRequest.requestType) {
      case RequestType.REQUEST_MIFID_PERMISSIONS:
        return this.translateService.instant('dpc-datool.requestTable.body.requestDetailsSummary.mifidPermissionRequest');

      case RequestType.REQUEST_USER_CREATION:
        return this.translateService.instant('dpc-datool.requestTable.body.requestDetailsSummary.userCreationRequest', {
          name: pendingRequest.requestDetails.firstName + ' ' + pendingRequest.requestDetails.lastName,
          birthdate: datePipe.transform(pendingRequest.requestDetails.birthday, 'dd.MM.yyyy')
        });

      case RequestType.REQUEST_POA_PROXY_CREATION:
        return this.translateService.instant('dpc-datool.requestTable.body.requestDetailsSummary.userCreationRequest', {
          name: pendingRequest.requestDetails.firstName + ' ' + pendingRequest.requestDetails.lastName,
          birthdate: pendingRequest.requestDetails.birthday
        });

      case RequestType.REQUEST_POA_PROXY_NDG_CREATION:
        return this.translateService.instant('dpc-datool.requestTable.body.requestDetailsSummary.userCreationRequest', {
          name: pendingRequest.requestDetails.firstName + ' ' + pendingRequest.requestDetails.lastName,
          birthdate: pendingRequest.requestDetails.birthday
        });

      case RequestType.REQUEST_POA_PROXY_CREATION_ADMIN:
        return this.translateService.instant('dpc-datool.requestTable.body.requestDetailsSummary.userCreationRequest', {
          name: pendingRequest.requestDetails.firstName + ' ' + pendingRequest.requestDetails.lastName,
          birthdate: pendingRequest.requestDetails.birthday
        });

      case RequestType.REQUEST_POA_PROXY_NDG_CREATION_ADMIN:
        return this.translateService.instant('dpc-datool.requestTable.body.requestDetailsSummary.userCreationRequest', {
          name: pendingRequest.requestDetails.firstName + ' ' + pendingRequest.requestDetails.lastName,
          birthdate: pendingRequest.requestDetails.birthday
        });

      case RequestType.REQUEST_USER_NDG_CREATION:
        return this.translateService.instant('dpc-datool.requestTable.body.requestDetailsSummary.userNdgCreationRequest', {
          name: pendingRequest.requestDetails.firstName + ' ' + pendingRequest.requestDetails.lastName,
          birthdate: datePipe.transform(pendingRequest.requestDetails.birthday, 'dd.MM.yyyy')
        });

      case RequestType.REQUEST_EBICS_USERS_DELETION:
        return this.translateService.instant('dpc-datool.requestTable.body.requestDetailsSummary.ebicsUsersDeletionRequest', {
          ebicsCustomerId: pendingRequest.requestDetails.ebicsCustomerId,
          ebicsUserId: pendingRequest.requestDetails.ebicsUsers[0].ebicsUserId,
          ebicsUserName: pendingRequest.requestDetails.ebicsUsers[0].fullName
        });

      case RequestType.REQUEST_EBICS_ACCOUNTS_DELETION:
        return this.translateService.instant('dpc-datool.requestTable.body.requestDetailsSummary.ebicsAccountsDeletionRequest', {
          ebicsCustomerId: pendingRequest.requestDetails.ebicsCustomerId,
          accountNumber: pendingRequest.requestDetails.accounts[0].accountNumber,
          bankCode: pendingRequest.requestDetails.accounts[0].bankCode,
          iban: pendingRequest.requestDetails.accounts[0].iban,
          bic: pendingRequest.requestDetails.accounts[0].bic
        });

      case RequestType.REQUEST_EBICS_ACCOUNT_CREATION:
        return this.translateService.instant('dpc-datool.requestTable.body.requestDetailsSummary.ebicsAccountCreationRequest', {
          ebicsCustomerId: pendingRequest.requestDetails.ebicsCustomerId,
          accountNumber: pendingRequest.requestDetails.account.accountNumber,
          bankCode: pendingRequest.requestDetails.account.bankCode,
          iban: pendingRequest.requestDetails.account.iban,
          bic: pendingRequest.requestDetails.account.bic,
          currency: pendingRequest.requestDetails.account.currency
        });

      case RequestType.REQUEST_EBICS_USER_CREATION:
        return this.translateService.instant('dpc-datool.requestTable.body.requestDetailsSummary.ebicsUserCreationRequest', {
          ebicsUserName: pendingRequest.requestDetails.fullName,
          ebicsUserNDG: pendingRequest.requestDetails.ndg
        });

      case RequestType.REQUEST_EBICS_CUSTOMER_CREATION:
        return this.translateService.instant('dpc-datool.requestTable.body.requestDetailsSummary.ebicsCustomerCreationRequest', {
          ebicsCustomerName: pendingRequest.requestDetails.customer.companyName,
          ebicsCustomerNdg: pendingRequest.requestDetails.customer.customerNdg
        });

      case RequestType.REQUEST_UPDATE_UCPAYFX_USERS_PERMISSIONS:
        return this.translateService.instant('dpc-datool.requestTable.body.requestDetailsSummary.updateUcpayfxUsersPermissionsRequest');

      case RequestType.REQUEST_UPDATE_UCHEDGE_USERS_PERMISSIONS:
        return this.translateService.instant('dpc-datool.requestTable.body.requestDetailsSummary.updateUchedgeUsersPermissionsRequest');

      case RequestType.REQUEST_ADD_USERS_TO_UCPAYFX:
        return this.translateService.instant('dpc-datool.requestTable.body.requestDetailsSummary.addUsersToUcpayfxRequest');

      case RequestType.REQUEST_ADD_USERS_TO_UCHEDGE:
        return this.translateService.instant('dpc-datool.requestTable.body.requestDetailsSummary.addUsersToUchedgeRequest');
    }
  }

  updatePendingRequests() {
    this.subscriptions.updateRequests = this.pendingRequestsService
      .getRequests(this.selectedCustomer.customerNdg, this.isInHistoryPage)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          return throwError(() => new Error(`Something bad happened: ${error}`));
        })
      )
      .subscribe((requests) => {
        this.pendingRequests = requests
          .filter((request) => request.requestStatus === RequestStatus.REQUEST_PENDING)
          .sort((a, b) => b.createdAt - a.createdAt);

        this.filteredRequests = [...this.pendingRequests];
        this.sortedRequests = [...this.pendingRequests];
        this.calculatePagination();

        this.ongetRequestsNumber.emit(this.pendingRequests.length);
        let ascClasses = this.elementRef.nativeElement.querySelectorAll('.asc');
        let descClasses = this.elementRef.nativeElement.querySelectorAll('.desc');
        ascClasses.forEach((el) => el.classList.remove('asc'));
        descClasses.forEach((el) => el.classList.remove('desc'));
        this.sortDirection === 0;
      });
  }

  openDetailsModal(pendingRequest) {
    const options: NgbModalOptions = this.isInHistoryPage
      ? { backdrop: true, centered: true, windowClass: 'notification-modal-custom', size: 'lg' }
      : { backdrop: 'static', centered: true, windowClass: 'notification-modal-custom', size: 'lg' };
    const modalRef = this.modalService.open(PendingRequestModalComponent, options);
    modalRef.componentInstance.selectedCustomer = this.selectedCustomer;
    modalRef.componentInstance.pendingRequest = pendingRequest;
    modalRef.componentInstance.onConfirm.subscribe(($e) => {
      this.updatePendingRequests();
    });
  }

  convertTimestampToDate(timestamp) {
    const date = new Date(timestamp * 1000);
    const options: Intl.DateTimeFormatOptions = {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric',
      hour: '2-digit',
      minute: '2-digit',
      timeZone: 'CET'
    };
    const formattedDate = date.toLocaleString('de-DE', options).replace(/\./g, '/').replace(/,/g, ` -`) + ' CET';
    return formattedDate;
  }

  sortColumn(property: string) {
    const index = this.getColumnIndex(property);

    if (this.sortColumnIndex === index) {
      this.sortDirection++;

      if (this.sortDirection > 2) {
        this.sortDirection = 0;
      }
    } else {
      this.sortColumnIndex = index;
      this.sortDirection = 1;
    }

    if (this.sortDirection === 1) {
      this.filteredRequests.sort((a, b) => this.compareValues(a[property], b[property], 1));
      this.sortedRequests.sort((a, b) => this.compareValues(a[property], b[property], 1));
    } else if (this.sortDirection === 2) {
      this.filteredRequests.sort((a, b) => this.compareValues(a[property], b[property], -1));
      this.sortedRequests.sort((a, b) => this.compareValues(a[property], b[property], -1));
    } else {
      let originalOrder = this.pendingRequests.filter((req) => {
        return this.filteredRequests.some((fReq) => {
          return JSON.stringify(fReq) === JSON.stringify(req);
        });
      });
      this.filteredRequests = [...originalOrder];
      this.sortedRequests = [...this.pendingRequests];
    }
  }

  getColumnIndex(property: string): number {
    const properties = Object.keys(this.pendingRequests[0]);
    return properties.indexOf(property);
  }

  compareValues(a: string | number, b: string | number, direction: number): number {
    if (a < b) {
      return -1 * direction;
    } else if (a > b) {
      return 1 * direction;
    } else {
      return 0;
    }
  }

  getColumnClass(property: string): string {
    const index = this.getColumnIndex(property);

    if (this.sortColumnIndex === index) {
      if (this.sortDirection === 1) {
        return 'asc';
      } else if (this.sortDirection === 2) {
        return 'desc';
      } else if (this.sortDirection === 0) {
        return '';
      }
    }

    return '';
  }

  calculatePagination(): void {
    const totalItems = this.filteredRequests.length;
    this.totalPages = Math.ceil(totalItems / this.itemsPerPage);

    this.paginationArray = Array.from({ length: this.totalPages }, (_, i) => i + 1);
  }

  changePage(page: number): void {
    this.currentPage = page;
  }

  getVisibleRequests(): Request[] {
    if (this.isInHistoryPage) {
      const startIndex = (this.currentPage - 1) * this.itemsPerPage;
      const endIndex = startIndex + this.itemsPerPage;
      return this.filteredRequests.slice(startIndex, endIndex);
    }
    return this.filteredRequests;
  }

  nextPage(): void {
    if (this.currentPage < this.paginationArray.length) {
      this.currentPage++;
    }
  }

  previousPage(): void {
    if (this.currentPage > 1) {
      this.currentPage--;
    }
  }

  getSearchName(searchName: string) {
    this.requestFilters.searchName = searchName;
    this.onFilter();
  }

  getFilters(requestFilters: RequestFilters) {
    this.requestFilters = { ...this.requestFilters, ...requestFilters };
    this.onFilter();
  }

  onFilter() {
    let requests = [...this.sortedRequests];

    let searchRegExp = new RegExp(this.requestFilters.searchName, 'i');

    if (this.requestFilters.searchName) {
      requests = requests.filter((request) => searchRegExp.test(request.approverName) || searchRegExp.test(request.initiatorName));
    }

    if (this.requestFilters.statuses.length) {
      requests = requests.filter((request) => this.requestFilters.statuses.includes(request.requestStatus));
    }

    if (this.requestFilters.tool) {
      requests = requests.filter((request) => this.requestFilters.tool === request.tool.toString());
    }

    if (this.requestFilters.startDateStamp && this.requestFilters.endDateStamp) {
      requests = requests.filter((request) => {
        const requestTimestamp = new Date(request.createdAt * 1000);
        requestTimestamp.setHours(0, 0, 0, 0);

        return (
          requestTimestamp >= new Date(this.requestFilters.startDateStamp * 1000) &&
          requestTimestamp <= new Date(this.requestFilters.endDateStamp * 1000)
        );
      });
    }

    this.filteredRequests = requests;
    this.calculatePagination();

    this.currentPage = 1;
  }

  ngOnDestroy(): void {
    Object.keys(this.subscriptions).forEach((key) => {
      this.subscriptions[key].unsubscribe();
    });
  }
}
