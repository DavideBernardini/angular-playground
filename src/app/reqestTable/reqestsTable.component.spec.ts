import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbModal, NgbModalModule, NgbModalOptions, NgbModalRef, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { PendingRequestsTablelComponent } from './pending-requests-table.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgModule, ElementRef } from '@angular/core';
import { CoreModule } from 'app/@core/core.module';
import { PendingRequestModalComponent } from '../pending-request-modal/pending-request-modal.component';
import { of } from 'rxjs';
import { PendingRequestsService } from 'app/@core/services/pending-requests.service';
import { RouterTestingModule } from '@angular/router/testing';
import { AppAuthenticationService } from 'app/@core/services/app-authentication.service';
import { LOCAL_STORAGE_OBJECT_TOKEN } from '@qgp/gtb-angular-library/dist';
import { createLocalStorageMock } from '../../../../@core/tests/core.tests';
import { RequestFilters } from '../requests-filters/requests-filters.component';
import { RequestOriginTool, RequestStatus, RequestType } from 'app/@shared/models/pending-request.enum';
import { CustomerUserType, MicrositeCode, PermissionStatus, StatusActive } from 'app/@shared/models/customer-microsites.enum';
import { Request } from 'app/@shared/models/pending-request';

describe('PendingRequestsTablelComponent', () => {
  let component: PendingRequestsTablelComponent;
  let fixture: ComponentFixture<PendingRequestsTablelComponent>;
  let modalService: NgbModal;
  let mockModalRef = {
    componentInstance: {
      onConfirm: {
        subscribe: jasmine.createSpy('subscribe')
      },
      selectedCustomer: null,
      pendingRequest: null
    },
    result: Promise.resolve()
  } as NgbModalRef;
  const requestServiceSpy = jasmine.createSpyObj('PendingRequestsService', ['getRequests', 'updateRequests']);
  const requestsMock: Request[] = [
    {
      tool: RequestOriginTool.ACCOUNT_MANAGEMENT,
      requestId: 353543,
      requestType: RequestType.REQUEST_MIFID_PERMISSIONS,
      requestStatus: RequestStatus.REQUEST_PENDING,
      requestDetails: {
        fullName: 'mario rossi',
        roleName: 'MIFID_EDITOR',
        userNdg: '410491480',
        globalId: 'BM2241829876940372',
        permissionNames: ['Reconciliation_Read', 'ES_Read', 'TCFXO_Read', 'TCIRD_Read', 'TS_Read', 'VA_Read']
      },
      createdAt: 1,
      initiatorName: 'Christian Toms',
      approverName: 'Emily Franz',
      approver2Name: 'Emilio Fede',
      initiator: '410491480',
      approver: '163982511',
      approver2: '892892'
    },
    {
      tool: RequestOriginTool.PRODUCT_USER_MANAGEMENT,
      requestId: 12321,
      requestType: RequestType.REQUEST_MIFID_PERMISSIONS,
      requestStatus: RequestStatus.REQUEST_PENDING,
      requestDetails: {
        fullName: 'mario rossi',
        roleName: 'MIFID_EDITOR',
        userNdg: '410491480',
        globalId: 'BM2241829876940372',
        permissionNames: ['Reconciliation_Read', 'ES_Read', 'TCFXO_Read', 'TCIRD_Read', 'TS_Read', 'VA_Read']
      },
      createdAt: 3,
      initiatorName: 'Christian Toms',
      approverName: 'Emily Franz',
      approver2Name: 'Emilio Fede',
      initiator: '410491480',
      approver: '163982511',
      approver2: '892892'
    },
    {
      tool: RequestOriginTool.PRODUCT_USER_MANAGEMENT,
      requestId: 77777,
      requestType: RequestType.REQUEST_MIFID_PERMISSIONS,
      requestStatus: RequestStatus.REQUEST_PENDING,
      requestDetails: {
        fullName: 'mario rossi',
        roleName: 'MIFID_EDITOR',
        userNdg: '410491480',
        globalId: 'BM2241829876940372',
        permissionNames: ['Reconciliation_Read', 'ES_Read', 'TCFXO_Read', 'TCIRD_Read', 'TS_Read', 'VA_Read']
      },
      createdAt: 2,
      initiatorName: 'Christian Toms',
      approverName: 'Emily Franz',
      approver2Name: 'Emilio Fede',
      initiator: '410491480',
      approver: '163982511',
      approver2: '892892'
    }
  ];
  const selectedCustomer = {
    customerNdg: '12345',
    customerName: 'aaaa',
    microsites: [{ code: MicrositeCode.ACCOUNT_MANAGEMENT, status: StatusActive.PERMISSION_STATUS_ACTIVE }],
    permissionStatus: PermissionStatus.PERMISSION_CAN_REQUEST,
    typeUserLogged: [CustomerUserType.TYPE_POR]
  };
  let mockPendingRequest;

  beforeEach(async () => {
    mockPendingRequest = {
      tool: RequestOriginTool.PRODUCT_USER_MANAGEMENT,
      requestId: 2262,
      requestType: '',
      requestDetails: {},
      requestStatus: RequestStatus.REQUEST_PENDING,
      createdAt: 1682608654,
      initiatorName: 'Christian Toms',
      approverName: 'Emily Franz',
      approver2Name: 'Emilio Fede',
      initiator: '410491480',
      approver: '163982511',
      approver2: '892892'
    };

    await TestBed.configureTestingModule({
      declarations: [PendingRequestsTablelComponent],
      imports: [TranslateModule.forRoot(), NgbModule, RouterTestingModule, HttpClientTestingModule],
      providers: [
        RouterTestingModule,
        TranslateService,
        { provide: AppAuthenticationService, useValue: {} },
        {
          provide: LOCAL_STORAGE_OBJECT_TOKEN,
          useValue: createLocalStorageMock({})
        },
        { provide: PendingRequestsService, useValue: requestServiceSpy }
      ]
    }).compileComponents();

    modalService = TestBed.inject(NgbModal);
    fixture = TestBed.createComponent(PendingRequestsTablelComponent);
    component = fixture.componentInstance;
    component.pendingRequests = requestsMock;
    component.sortedRequests = requestsMock;
    component.filteredRequests = requestsMock;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize component with selected customer', () => {
    component.selectedCustomer = selectedCustomer;

    const requests: Request[] = requestsMock;
    (component as any).pendingRequestsService.getRequests.and.returnValue(of(requests));

    component.ngOnInit();

    expect(component.pendingRequests).toEqual(requests);
    expect(component.filteredRequests).toEqual(requests);
    expect(component.sortedRequests).toEqual(requests);
  });
  it('should initialize component without selected customer', () => {
    component.pendingRequests = [];
    component.sortedRequests = [];
    component.filteredRequests = [];
    component.selectedCustomer = null;

    component.ngOnInit();

    expect(component.pendingRequests).toEqual([]);
    expect(component.filteredRequests).toEqual([]);
    expect(component.sortedRequests).toEqual([]);
  });

  it('should stay in history page', () => {
    spyOnProperty((component as any).router, 'url', 'get').and.returnValue('/da-tool/pending-requests-history');

    component.ngOnInit();

    expect(component.isInHistoryPage).toBeTrue();
  });

  it('should update pending requests', () => {
    component.selectedCustomer = selectedCustomer;
    const updatedRequests: Request[] = requestsMock;
    (component as any).pendingRequestsService.getRequests.and.returnValue(of(requestsMock));
    (component as any).pendingRequestsService.updateRequests.and.returnValue(of(updatedRequests));

    component.updatePendingRequests();

    expect(component.pendingRequests).toEqual(updatedRequests.sort((a, b) => b.createdAt - a.createdAt));
  });

  it('should open the details modal and subscribe to onConfirm event', () => {
    const pendingRequest = { ...requestsMock[0] };
    const options: NgbModalOptions = { backdrop: 'static', centered: true, windowClass: 'notification-modal-custom', size: 'lg' };

    spyOn(modalService, 'open').and.returnValue(mockModalRef);
    spyOn(component, 'updatePendingRequests');

    component.openDetailsModal(pendingRequest);

    expect(modalService.open).toHaveBeenCalled();
    expect(modalService.open).toHaveBeenCalledWith(PendingRequestModalComponent, options);
    expect(mockModalRef.componentInstance.selectedCustomer).toBe(component.selectedCustomer);
    expect(mockModalRef.componentInstance.pendingRequest).toEqual(pendingRequest);
    expect(mockModalRef.componentInstance.onConfirm.subscribe).toHaveBeenCalled();
    expect(component.updatePendingRequests).not.toHaveBeenCalled();

    const confirmCallback = mockModalRef.componentInstance.onConfirm.subscribe.calls.mostRecent().args[0];
    confirmCallback();

    fixture.detectChanges();

    expect(component.updatePendingRequests).toHaveBeenCalled();
  });

  it('should return the correct request details for MIFID_PERMISSIONS', () => {
    mockPendingRequest.requestType = RequestType.REQUEST_MIFID_PERMISSIONS;
    const translateinstantSpy = spyOn((component as any).translateService, 'instant').and.returnValue('Translated request details');

    const result = component.addRequestDetails(mockPendingRequest);

    expect(translateinstantSpy).toHaveBeenCalledWith('dpc-datool.requestTable.body.requestDetailsSummary.mifidPermissionRequest');
    expect(result).toBe('Translated request details');
  });

  it('should return the correct request details for ADD_USERS_TO_UCPAYFX', () => {
    mockPendingRequest.requestType = RequestType.REQUEST_ADD_USERS_TO_UCPAYFX;

    const translateinstantSpy = spyOn((component as any).translateService, 'instant').and.returnValue('Translated request details');

    const result = component.addRequestDetails(mockPendingRequest);

    expect(translateinstantSpy).toHaveBeenCalledWith('dpc-datool.requestTable.body.requestDetailsSummary.addUsersToUcpayfxRequest');
    expect(result).toBe('Translated request details');
  });

  it('should return the correct request details for UPDATE_UCPAYFX_USERS_PERMISSIONS', () => {
    mockPendingRequest.requestType = RequestType.REQUEST_UPDATE_UCPAYFX_USERS_PERMISSIONS;

    const translateinstantSpy = spyOn((component as any).translateService, 'instant').and.returnValue('Translated request details');

    const result = component.addRequestDetails(mockPendingRequest);

    expect(translateinstantSpy).toHaveBeenCalledWith(
      'dpc-datool.requestTable.body.requestDetailsSummary.updateUcpayfxUsersPermissionsRequest'
    );
    expect(result).toBe('Translated request details');
  });

  it('should return the correct request details for UPDATE_UCHEDGE_USERS_PERMISSIONS', () => {
    mockPendingRequest.requestType = RequestType.REQUEST_UPDATE_UCHEDGE_USERS_PERMISSIONS;

    const translateinstantSpy = spyOn((component as any).translateService, 'instant').and.returnValue('Translated request details');

    const result = component.addRequestDetails(mockPendingRequest);

    expect(translateinstantSpy).toHaveBeenCalledWith(
      'dpc-datool.requestTable.body.requestDetailsSummary.updateUchedgeUsersPermissionsRequest'
    );
    expect(result).toBe('Translated request details');
  });

  it('should return the correct request details for ADD_USERS_TO_UCHEDGE', () => {
    mockPendingRequest.requestType = RequestType.REQUEST_ADD_USERS_TO_UCHEDGE;

    const translateinstantSpy = spyOn((component as any).translateService, 'instant').and.returnValue('Translated request details');

    const result = component.addRequestDetails(mockPendingRequest);

    expect(translateinstantSpy).toHaveBeenCalledWith('dpc-datool.requestTable.body.requestDetailsSummary.addUsersToUchedgeRequest');
    expect(result).toBe('Translated request details');
  });
  it('should return the correct request details for USER_CREATION', () => {
    mockPendingRequest.requestType = RequestType.REQUEST_USER_CREATION;
    mockPendingRequest.requestDetails = { firstName: 'karl', lastName: 'string', birthday: '1991-12-19' };

    const translateinstantSpy = spyOn((component as any).translateService, 'instant').and.returnValue('Translated request details');

    const result = component.addRequestDetails(mockPendingRequest);

    expect(translateinstantSpy).toHaveBeenCalledWith(
      'dpc-datool.requestTable.body.requestDetailsSummary.userCreationRequest',
      Object({ name: 'karl string', birthdate: '19.12.1991' })
    );
    expect(result).toBe('Translated request details');
  });
  it('should return the correct request details for EBICS_ACCOUNT_CREATION', () => {
    mockPendingRequest.requestType = RequestType.REQUEST_EBICS_ACCOUNT_CREATION;
    mockPendingRequest.requestDetails = {
      ebicsCustomerId: '123',
      account: { accountNumber: '4567', bankCode: '800900313', iban: 'IT6786868', bic: 'test', currency: 'JPY' }
    };

    const translateinstantSpy = spyOn((component as any).translateService, 'instant').and.returnValue('Translated request details');

    const result = component.addRequestDetails(mockPendingRequest);

    expect(translateinstantSpy).toHaveBeenCalledWith(
      'dpc-datool.requestTable.body.requestDetailsSummary.ebicsAccountCreationRequest',
      Object({ ebicsCustomerId: '123', accountNumber: '4567', bankCode: '800900313', iban: 'IT6786868', bic: 'test', currency: 'JPY' })
    );
    expect(result).toBe('Translated request details');
  });
  it('should return the correct request details for EBICS_USER_CREATION', () => {
    mockPendingRequest.requestType = RequestType.REQUEST_EBICS_USER_CREATION;
    mockPendingRequest.requestDetails = {
      fullName: 'mario string',
      ndg: 12345678
    };

    const translateinstantSpy = spyOn((component as any).translateService, 'instant').and.returnValue('Translated request details');

    const result = component.addRequestDetails(mockPendingRequest);

    expect(translateinstantSpy).toHaveBeenCalledWith(
      'dpc-datool.requestTable.body.requestDetailsSummary.ebicsUserCreationRequest',
      Object({ ebicsUserName: 'mario string', ebicsUserNDG: 12345678 })
    );
    expect(result).toBe('Translated request details');
  });
  it('should return the correct request details for EBICS_CUSTOMER_CREATION', () => {
    mockPendingRequest.requestType = RequestType.REQUEST_EBICS_CUSTOMER_CREATION;
    mockPendingRequest.requestDetails = {
      customer: {
        companyName: 'Proraso',
        customerNdg: 12345678
      }
    };

    const translateinstantSpy = spyOn((component as any).translateService, 'instant').and.returnValue('Translated request details');

    const result = component.addRequestDetails(mockPendingRequest);

    expect(translateinstantSpy).toHaveBeenCalledWith(
      'dpc-datool.requestTable.body.requestDetailsSummary.ebicsCustomerCreationRequest',
      Object({ ebicsCustomerName: 'Proraso', ebicsCustomerNdg: 12345678 })
    );
    expect(result).toBe('Translated request details');
  });
  it('should return the correct request details for EBICS_ACCOUNTS_DELETION', () => {
    mockPendingRequest.requestType = RequestType.REQUEST_EBICS_ACCOUNTS_DELETION;
    mockPendingRequest.requestDetails = {
      ebicsCustomerId: '123',
      accounts: [{ accountNumber: '4567', bankCode: '800900313', iban: 'IT6786868', bic: 'test' }]
    };

    const translateinstantSpy = spyOn((component as any).translateService, 'instant').and.returnValue('Translated request details');

    const result = component.addRequestDetails(mockPendingRequest);

    expect(translateinstantSpy).toHaveBeenCalledWith(
      'dpc-datool.requestTable.body.requestDetailsSummary.ebicsAccountsDeletionRequest',
      Object({ ebicsCustomerId: '123', accountNumber: '4567', bankCode: '800900313', iban: 'IT6786868', bic: 'test' })
    );
    expect(result).toBe('Translated request details');
  });
  it('should return the correct request details for EBICS_USERS_DELETION', () => {
    mockPendingRequest.requestType = RequestType.REQUEST_EBICS_USERS_DELETION;
    mockPendingRequest.requestDetails = {
      ebicsCustomerId: '123',
      ebicsUsers: [{ ebicsUserId: '4567', ndg: '892', fullName: 'giovanni rana' }]
    };

    const translateinstantSpy = spyOn((component as any).translateService, 'instant').and.returnValue('Translated request details');

    const result = component.addRequestDetails(mockPendingRequest);

    expect(translateinstantSpy).toHaveBeenCalledWith(
      'dpc-datool.requestTable.body.requestDetailsSummary.ebicsUsersDeletionRequest',
      Object({ ebicsCustomerId: '123', ebicsUserId: '4567', ebicsUserName: 'giovanni rana' })
    );
    expect(result).toBe('Translated request details');
  });
  it('should return the correct request details for REQUEST_POA_PROXY_CREATION', () => {
    mockPendingRequest.requestType = RequestType.REQUEST_POA_PROXY_CREATION;
    mockPendingRequest.requestDetails = { firstName: 'karl', lastName: 'string', birthday: '17.04.63' };

    const translateinstantSpy = spyOn((component as any).translateService, 'instant').and.returnValue('Translated request details');

    const result = component.addRequestDetails(mockPendingRequest);

    expect(translateinstantSpy).toHaveBeenCalledWith(
      'dpc-datool.requestTable.body.requestDetailsSummary.userCreationRequest',
      Object({ name: 'karl string', birthdate: '17.04.63' })
    );
    expect(result).toBe('Translated request details');
  });
  it('should return the correct request details for REQUEST_POA_PROXY_NDG_CREATION', () => {
    mockPendingRequest.requestType = RequestType.REQUEST_POA_PROXY_NDG_CREATION;
    mockPendingRequest.requestDetails = { firstName: 'karl', lastName: 'string', birthday: '17.04.63' };

    const translateinstantSpy = spyOn((component as any).translateService, 'instant').and.returnValue('Translated request details');

    const result = component.addRequestDetails(mockPendingRequest);

    expect(translateinstantSpy).toHaveBeenCalledWith(
      'dpc-datool.requestTable.body.requestDetailsSummary.userCreationRequest',
      Object({ name: 'karl string', birthdate: '17.04.63' })
    );
    expect(result).toBe('Translated request details');
  });
  it('should return the correct request details for REQUEST_POA_PROXY_CREATION_ADMIN', () => {
    mockPendingRequest.requestType = RequestType.REQUEST_POA_PROXY_CREATION_ADMIN;
    mockPendingRequest.requestDetails = { firstName: 'karl', lastName: 'string', birthday: '17.04.63' };

    const translateinstantSpy = spyOn((component as any).translateService, 'instant').and.returnValue('Translated request details');

    const result = component.addRequestDetails(mockPendingRequest);

    expect(translateinstantSpy).toHaveBeenCalledWith(
      'dpc-datool.requestTable.body.requestDetailsSummary.userCreationRequest',
      Object({ name: 'karl string', birthdate: '17.04.63' })
    );
    expect(result).toBe('Translated request details');
  });
  it('should return the correct request details for REQUEST_POA_PROXY_NDG_CREATION_ADMIN', () => {
    mockPendingRequest.requestType = RequestType.REQUEST_POA_PROXY_NDG_CREATION_ADMIN;
    mockPendingRequest.requestDetails = { firstName: 'karl', lastName: 'string', birthday: '17.04.63' };

    const translateinstantSpy = spyOn((component as any).translateService, 'instant').and.returnValue('Translated request details');

    const result = component.addRequestDetails(mockPendingRequest);

    expect(translateinstantSpy).toHaveBeenCalledWith(
      'dpc-datool.requestTable.body.requestDetailsSummary.userCreationRequest',
      Object({ name: 'karl string', birthdate: '17.04.63' })
    );
    expect(result).toBe('Translated request details');
  });

  it('should return the correct request details for USER_NDG_CREATION', () => {
    mockPendingRequest.requestType = RequestType.REQUEST_USER_NDG_CREATION;
    mockPendingRequest.requestDetails = { firstName: 'karl', lastName: 'string', birthday: '1991-12-19' };

    const translateinstantSpy = spyOn((component as any).translateService, 'instant').and.returnValue('Translated request details');

    const result = component.addRequestDetails(mockPendingRequest);

    expect(translateinstantSpy).toHaveBeenCalledWith(
      'dpc-datool.requestTable.body.requestDetailsSummary.userNdgCreationRequest',
      Object({ name: 'karl string', birthdate: '19.12.1991' })
    );
    expect(result).toBe('Translated request details');
  });

  it('should convert timestamp to formatted date', () => {
    const timestamp = 1621716000;
    const formattedDate = component.convertTimestampToDate(timestamp);
    expect(formattedDate).toBe('22/05/2021 - 22:40 CET');
  });

  it('should sort the table column', () => {
    const createdAtProperty = 'createdAt';

    component.sortColumn(createdAtProperty);

    expect(component.filteredRequests).toEqual([
      {
        tool: RequestOriginTool.ACCOUNT_MANAGEMENT,
        requestId: 353543,
        requestType: RequestType.REQUEST_MIFID_PERMISSIONS,
        requestStatus: RequestStatus.REQUEST_PENDING,
        requestDetails: {
          fullName: 'mario rossi',
          roleName: 'MIFID_EDITOR',
          userNdg: '410491480',
          globalId: 'BM2241829876940372',
          permissionNames: ['Reconciliation_Read', 'ES_Read', 'TCFXO_Read', 'TCIRD_Read', 'TS_Read', 'VA_Read']
        },
        createdAt: 1,
        initiatorName: 'Christian Toms',
        approverName: 'Emily Franz',
        approver2Name: 'Emilio Fede',
        initiator: '410491480',
        approver: '163982511',
        approver2: '892892'
      },
      {
        tool: RequestOriginTool.PRODUCT_USER_MANAGEMENT,
        requestId: 77777,
        requestType: RequestType.REQUEST_MIFID_PERMISSIONS,
        requestStatus: RequestStatus.REQUEST_PENDING,
        requestDetails: {
          fullName: 'mario rossi',
          roleName: 'MIFID_EDITOR',
          userNdg: '410491480',
          globalId: 'BM2241829876940372',
          permissionNames: ['Reconciliation_Read', 'ES_Read', 'TCFXO_Read', 'TCIRD_Read', 'TS_Read', 'VA_Read']
        },
        createdAt: 2,
        initiatorName: 'Christian Toms',
        approverName: 'Emily Franz',
        approver2Name: 'Emilio Fede',
        initiator: '410491480',
        approver: '163982511',
        approver2: '892892'
      },
      {
        tool: RequestOriginTool.PRODUCT_USER_MANAGEMENT,
        requestId: 12321,
        requestType: RequestType.REQUEST_MIFID_PERMISSIONS,
        requestStatus: RequestStatus.REQUEST_PENDING,
        requestDetails: {
          fullName: 'mario rossi',
          roleName: 'MIFID_EDITOR',
          userNdg: '410491480',
          globalId: 'BM2241829876940372',
          permissionNames: ['Reconciliation_Read', 'ES_Read', 'TCFXO_Read', 'TCIRD_Read', 'TS_Read', 'VA_Read']
        },
        createdAt: 3,
        initiatorName: 'Christian Toms',
        approverName: 'Emily Franz',
        approver2Name: 'Emilio Fede',
        initiator: '410491480',
        approver: '163982511',
        approver2: '892892'
      }
    ]);

    component.sortColumn(createdAtProperty);

    expect(component.filteredRequests).toEqual([
      {
        tool: RequestOriginTool.PRODUCT_USER_MANAGEMENT,
        requestId: 12321,
        requestType: RequestType.REQUEST_MIFID_PERMISSIONS,
        requestStatus: RequestStatus.REQUEST_PENDING,
        requestDetails: {
          fullName: 'mario rossi',
          roleName: 'MIFID_EDITOR',
          userNdg: '410491480',
          globalId: 'BM2241829876940372',
          permissionNames: ['Reconciliation_Read', 'ES_Read', 'TCFXO_Read', 'TCIRD_Read', 'TS_Read', 'VA_Read']
        },
        createdAt: 3,
        initiatorName: 'Christian Toms',
        approverName: 'Emily Franz',
        approver2Name: 'Emilio Fede',
        initiator: '410491480',
        approver: '163982511',
        approver2: '892892'
      },
      {
        tool: RequestOriginTool.PRODUCT_USER_MANAGEMENT,
        requestId: 77777,
        requestType: RequestType.REQUEST_MIFID_PERMISSIONS,
        requestStatus: RequestStatus.REQUEST_PENDING,
        requestDetails: {
          fullName: 'mario rossi',
          roleName: 'MIFID_EDITOR',
          userNdg: '410491480',
          globalId: 'BM2241829876940372',
          permissionNames: ['Reconciliation_Read', 'ES_Read', 'TCFXO_Read', 'TCIRD_Read', 'TS_Read', 'VA_Read']
        },
        createdAt: 2,
        initiatorName: 'Christian Toms',
        approverName: 'Emily Franz',
        approver2Name: 'Emilio Fede',
        initiator: '410491480',
        approver: '163982511',
        approver2: '892892'
      },
      {
        tool: RequestOriginTool.ACCOUNT_MANAGEMENT,
        requestId: 353543,
        requestType: RequestType.REQUEST_MIFID_PERMISSIONS,
        requestStatus: RequestStatus.REQUEST_PENDING,
        requestDetails: {
          fullName: 'mario rossi',
          roleName: 'MIFID_EDITOR',
          userNdg: '410491480',
          globalId: 'BM2241829876940372',
          permissionNames: ['Reconciliation_Read', 'ES_Read', 'TCFXO_Read', 'TCIRD_Read', 'TS_Read', 'VA_Read']
        },
        createdAt: 1,
        initiatorName: 'Christian Toms',
        approverName: 'Emily Franz',
        approver2Name: 'Emilio Fede',
        initiator: '410491480',
        approver: '163982511',
        approver2: '892892'
      }
    ]);

    component.sortColumn(createdAtProperty);
    expect(component.filteredRequests).toEqual(requestsMock);
  });

  it('should get the index of the column in the table', () => {
    const index = component.getColumnIndex('createdAt');

    expect(index).toBe(5);
  });

  it('should get column class "asc"', () => {
    component.sortColumnIndex = 5;
    component.sortDirection = 1;
    let columnClass = component.getColumnClass('createdAt');
    expect(columnClass).toBe('asc');
  });

  it('should get column class "desc"', () => {
    component.sortColumnIndex = 5;
    component.sortDirection = 2;
    let columnClass = component.getColumnClass('createdAt');
    expect(columnClass).toBe('desc');
  });

  it('should not get column class ', () => {
    component.sortColumnIndex = 5;
    component.sortDirection = 0;
    let columnClass = component.getColumnClass('createdAt');
    expect(columnClass).toBe('');
  });

  it('should calculate pagination', () => {
    component.itemsPerPage = 2;
    component.calculatePagination();
    expect(component.totalPages).toEqual(2);
    expect(component.paginationArray).toEqual([1, 2]);
  });

  it('should change the current page', () => {
    component.changePage(2);
    expect(component.currentPage).toEqual(2);
  });

  it('should return the visible requests', () => {
    component.isInHistoryPage = true;
    component.currentPage = 2;
    component.itemsPerPage = 2;
    const visibleRequests = component.getVisibleRequests();
    expect(visibleRequests.length).toEqual(1);
    expect(visibleRequests[0]).toEqual(requestsMock[2]);
  });

  it('should move to the next page if available', () => {
    component.currentPage = 1;
    component.paginationArray = [1, 2, 3];
    component.nextPage();
    expect(component.currentPage).toEqual(2);
  });
  it('should move to the previous page if available', () => {
    component.currentPage = 2;
    component.previousPage();
    expect(component.currentPage).toEqual(1);
  });
  it('should set the search name and call onFilter()', () => {
    spyOn(component, 'onFilter');
    component.getSearchName('mario');
    expect(component.requestFilters.searchName).toEqual('mario');
    expect(component.onFilter).toHaveBeenCalled();
  });
  it('should set the request filters and call onFilter()', () => {
    spyOn(component, 'onFilter');
    const filters: RequestFilters = {
      searchName: '',
      statuses: [RequestStatus.REQUEST_PENDING],
      tool: 'PM',
      startDateStamp: 12345,
      endDateStamp: 67890
    };
    component.getFilters(filters);
    expect(component.requestFilters).toEqual(filters);
    expect(component.onFilter).toHaveBeenCalled();
  });
  it('should filter the requests based on the filters and update pagination', () => {
    component.requestFilters = {
      statuses: [],
      tool: '',
      startDateStamp: 0,
      endDateStamp: 0,
      searchName: ''
    };
    component.onFilter();
    expect(component.filteredRequests.length).toEqual(3);
    expect(component.paginationArray.length).toEqual(1);
    expect(component.currentPage).toEqual(1);
  });
  it('should compare values based on the direction', () => {
    const a = 'abc';
    const b = 'def';
    const ascendingDirection = 1;
    const descendingDirection = -1;
    expect(component.compareValues(a, b, ascendingDirection)).toEqual(-1);
    expect(component.compareValues(a, b, descendingDirection)).toEqual(1);
    expect(component.compareValues(a, a, ascendingDirection)).toEqual(0);
  });
  it('should filter requests based on searchName', () => {
    component.requestFilters.searchName = 'Emily';
    component.onFilter();

    expect(component.filteredRequests.length).toBe(3);
    expect(component.filteredRequests[0].approverName).toBe('Emily Franz');
    expect(component.filteredRequests[1].initiatorName).toBe('Christian Toms');
    expect(component.filteredRequests[1].initiatorName).toBe('Christian Toms');
  });
  it('should filter requests based on statuses', () => {
    component.requestFilters.statuses = [RequestStatus.REQUEST_PENDING];
    component.onFilter();

    expect(component.filteredRequests.length).toBe(3);

    component.requestFilters.statuses = [RequestStatus.REQUEST_APPROVED, RequestStatus.REQUEST_ABORTED];
    component.onFilter();

    expect(component.filteredRequests.length).toBe(0);
  });
  it('should filter requests based on tool', () => {
    component.requestFilters.tool = 'PM';
    component.onFilter();

    expect(component.filteredRequests.length).toBe(2);

    component.requestFilters.tool = 'AM';
    component.onFilter();

    expect(component.filteredRequests.length).toBe(1);
  });
  it('should filter requests based on startDateStamp and endDateStamp', () => {
    component.requestFilters.startDateStamp = 1632000000;
    component.requestFilters.endDateStamp = 1693839035;

    component.onFilter();

    expect(component.filteredRequests.length).toBe(0);
  });
  it('should test ngOnInit', () => {
    spyOn((component as any).customerMicrositesPermissionService.selectedCustomer$, 'subscribe').and.callFake(() => {
      component.filteredRequests = [...component.pendingRequests];
      component.sortedRequests = [...component.pendingRequests];
      component.calculatePagination();

      component.areRequestsLoaded = true;

      component.ongetRequestsNumber.emit(component.pendingRequests.length);
    });
    spyOn(component, 'calculatePagination');

    component.ngOnInit();

    expect(component.pendingRequests).toEqual(requestsMock);
    expect(component.calculatePagination).toHaveBeenCalled();
    expect(component.filteredRequests).toEqual(component.pendingRequests);
    expect(component.sortedRequests).toEqual(component.pendingRequests);
    expect(component.areRequestsLoaded).toBeTrue();
  });
});
