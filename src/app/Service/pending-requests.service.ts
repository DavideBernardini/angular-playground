import { map } from 'rxjs/operators';
import { Observable, combineLatest } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { AppAuthenticationService } from './app-authentication.service';
import {
  Request,
  RequestAddUsersToUchedgeDetails,
  RequestAddUsersToUcpayfxDetails,
  RequestMifidPermissionDetails,
  RequestUpdateUchedgeUsersPermissionsDetails,
  RequestUpdateUcpayfxUsersPermissionsDetails,
  RequestUsers,
  RequestWithoutNames,
  User,
  UserName
} from 'app/@shared/models/pending-request';
import { RequestStatus, RequestType } from 'app/@shared/models/pending-request.enum';

@Injectable({
  providedIn: 'root'
})
export class PendingRequestsService {
  constructor(private http: HttpClient, private auth: AppAuthenticationService) { }

  getRequstsWithoutNames(customerNdg: string, isInHistory: boolean): Observable<RequestWithoutNames[]> {
    if (isInHistory) {
      const baseRequests = this.http
        .get<{ requests: RequestWithoutNames[] }>(environment.getValue('requestHistoryPath', null, 'WKP-PM-Tool'), {
          headers: { ...this.auth.getAuthHeaders(), context: customerNdg }
        })
        .pipe(map((body) => body.requests));

      return baseRequests;
    } else {
      const baseRequests = this.http
        .get<{ requests: RequestWithoutNames[] }>(environment.getValue('requestPath', null, 'WKP-PM-Tool'), {
          headers: { ...this.auth.getAuthHeaders(), context: customerNdg }
        })
        .pipe(map((body) => body.requests));

      return baseRequests;
    }
  }

  getCustomerUsers(customerNdg: string): Observable<User[]> {
    const customerUsers = this.http
      .get<{ customer: { users: User[] } }>(
        environment.getValue('customerPath', null, 'WKP-PM-Tool').replace('{customerNdg}', customerNdg),
        {
          headers: { ...this.auth.getAuthHeaders(), context: customerNdg }
        }
      )
      .pipe(map((body) => body.customer.users));

    return customerUsers;
  }

  getRequests(customerNdg: string, isInHistory: boolean): Observable<Request[]> {
    return combineLatest([this.getRequstsWithoutNames(customerNdg, isInHistory), this.getCustomerUsers(customerNdg)]).pipe(
      map((value) => {
        const requests = value[0];
        const customerUsers = value[1];

        return requests.map((request) => {
          const initiator = customerUsers.find((user) => user.ndg === request.initiator && user.ndg);
          const approver = customerUsers.find((user) => user.ndg === request.approver && user.ndg);
          const approver2 = customerUsers.find((user) => user.ndg === request.approver2 && user.ndg);

          let initiatorApproversNames;

          if (request.approver) {
            initiatorApproversNames = this.initiatorApproversNames(initiator, approver, approver2);
          } else {
            initiatorApproversNames = {
              initiatorName: initiator ? `${initiator.firstName} ${initiator.lastName}` : '-',
              approverName: initiator ? `${initiator.firstName} ${initiator.lastName}` : '-',
              approver2Name: `-`
            };
          }

          if (
            request.requestType === RequestType.REQUEST_ADD_USERS_TO_UCHEDGE ||
            request.requestType === RequestType.REQUEST_UPDATE_UCHEDGE_USERS_PERMISSIONS ||
            request.requestType === RequestType.REQUEST_ADD_USERS_TO_UCPAYFX ||
            request.requestType === RequestType.REQUEST_UPDATE_UCPAYFX_USERS_PERMISSIONS
          ) {
            const typedRequestDetails = request.requestDetails as
              | RequestUpdateUchedgeUsersPermissionsDetails
              | RequestUpdateUcpayfxUsersPermissionsDetails
              | RequestAddUsersToUchedgeDetails
              | RequestAddUsersToUcpayfxDetails;

            typedRequestDetails.users.map((user) => {
              const matchingUser = customerUsers.find((u) => u.portalId === user.globalId && u.portalId);

              if (matchingUser) {
                user.fullName = `${matchingUser.firstName} ${matchingUser.lastName}`;
              } else {
                user.fullName = '-';
              }
            });

            request.requestDetails = { ...request.requestDetails, ...typedRequestDetails };
          }

          if (request.requestType === RequestType.REQUEST_MIFID_PERMISSIONS) {
            const typedRequestDetails = request.requestDetails as RequestMifidPermissionDetails;
            const matchingUser = customerUsers.find((u) => u.portalId === typedRequestDetails.globalId);
            if (matchingUser) {
              typedRequestDetails.fullName = `${matchingUser.firstName} ${matchingUser.lastName}`;

              request.requestDetails = { ...request.requestDetails, ...typedRequestDetails };
            } else {
              typedRequestDetails.fullName = `-`;

              request.requestDetails = { ...request.requestDetails, ...typedRequestDetails };
            }
          }
          return { ...request, ...initiatorApproversNames };
        });
      })
    );
  }

  updateStatus(newStatus: RequestStatus, customerNdg, requestId) {
    const url = environment.getValue('updateRequestPath', null, 'WKP-PM-Tool').replace('{requestId}', requestId);
    const body = { status: newStatus };
    const headers = {
      headers: { ...this.auth.getAuthHeaders(), context: customerNdg }
    };
    return this.http.put(url, body, headers);
  }

  private initiatorApproversNames(initiator: UserName, approver: UserName, approver2: UserName): RequestUsers {
    return {
      initiatorName: initiator ? `${initiator.firstName} ${initiator.lastName}` : '-',
      approverName: approver ? `${approver.firstName} ${approver.lastName}` : '-',
      approver2Name: approver2 ? `${approver2.firstName} ${approver2.lastName}` : '-'
    };
  }
}
