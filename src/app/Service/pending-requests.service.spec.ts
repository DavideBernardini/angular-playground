import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { PendingRequestsService } from './pending-requests.service';
import { TestBed } from '@angular/core/testing';
import { AppAuthenticationService } from './app-authentication.service';
import { environment } from 'environments/environment';
import { Request, User } from 'app/@shared/models/pending-request';
import { RequestStatus, RequestType, RequestOriginTool } from 'app/@shared/models/pending-request.enum';

describe('PendingRequestsService', () => {
  let service: PendingRequestsService;
  let httpTestingController: HttpTestingController;
  let expectedRequests: Request[];
  let mockUsers: User[];
  let customerNdgExample: string;

  beforeEach(() => {
    customerNdgExample = '12345';
    mockUsers = [
      { ndg: 'ndg1', firstName: 'mario', lastName: 'rossi', portalId: '1234' },
      { ndg: 'ndg2', firstName: 'mario', lastName: 'bianchi', portalId: '1243' },
      { ndg: 'ndg3', firstName: 'mario', lastName: 'verdi', portalId: '1243' }
    ];
    expectedRequests = [
      {
        requestId: 1,
        requestType: RequestType.REQUEST_MIFID_PERMISSIONS,
        requestStatus: RequestStatus.REQUEST_PENDING,
        createdAt: 1620652292000,
        initiator: 'ndg1',
        approver: 'ndg2',
        approver2: 'ndg3',
        tool: RequestOriginTool.PRODUCT_USER_MANAGEMENT,
        initiatorName: 'mario rossi',
        approverName: 'mario bianchi',
        approver2Name: 'mario verdi',
        requestDetails: {
          fullName: 'Jhon Smith',
          roleName: 'MIFID_EDITOR',
          userNdg: '410491480',
          globalId: 'BM2241829876940372',
          permissionNames: ['Reconciliation_Read', 'ES_Read', 'TCFXO_Read', 'TCIRD_Read', 'TS_Read', 'VA_Read']
        }
      }
    ];

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        PendingRequestsService,
        {
          provide: AppAuthenticationService,
          useValue: {
            getAuthHeaders: () => ({})
          }
        }
      ]
    });
    service = TestBed.inject(PendingRequestsService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should retrieve the requests', () => {
    let requestsUrl = environment.getValue('requestPath', null, 'WKP-PM-Tool');
    let customerUrl = environment.getValue('customerPath', null, 'WKP-PM-Tool').replace('{customerNdg}', customerNdgExample);

    service.getRequests(customerNdgExample, false).subscribe((requests) => {
      expect(requests).toEqual(expectedRequests);
    });

    const requestsReq = httpTestingController.expectOne(requestsUrl);
    requestsReq.flush({ requests: expectedRequests });
    const usersReq = httpTestingController.expectOne(customerUrl);
    usersReq.flush({ customer: { users: mockUsers } });

    expect(requestsReq.request.method).toEqual('GET');
    expect(usersReq.request.method).toEqual('GET');
    httpTestingController.verify();
  });

  it('should retrieve the requests in history', () => {
    let requestsUrl = environment.getValue('requestHistoryPath', null, 'WKP-PM-Tool');
    let customerUrl = environment.getValue('customerPath', null, 'WKP-PM-Tool').replace('{customerNdg}', customerNdgExample);

    service.getRequests(customerNdgExample, true).subscribe((requests) => {
      expect(requests).toEqual(expectedRequests);
    });

    const requestsReq = httpTestingController.expectOne(requestsUrl);
    requestsReq.flush({ requests: expectedRequests });
    const usersReq = httpTestingController.expectOne(customerUrl);
    usersReq.flush({ customer: { users: mockUsers } });

    expect(requestsReq.request.method).toEqual('GET');
    expect(usersReq.request.method).toEqual('GET');
    httpTestingController.verify();
  });

  it('should test getRequests - no initiator and approver case', () => {
    let requestsUrl = environment.getValue('requestPath', null, 'WKP-PM-Tool');
    let customerUrl = environment.getValue('customerPath', null, 'WKP-PM-Tool').replace('{customerNdg}', customerNdgExample);

    expectedRequests[0].approverName = '-';
    expectedRequests[0].approver2Name = '-';
    expectedRequests[0].initiatorName = '-';
    service.getRequests(customerNdgExample, false).subscribe((requests) => {
      expect(requests).toEqual(expectedRequests);
    });

    const requestsReq = httpTestingController.expectOne(requestsUrl);
    requestsReq.flush({ requests: expectedRequests });
    const usersReq = httpTestingController.expectOne(customerUrl);
    usersReq.flush({ customer: { users: [] } });

    expect(requestsReq.request.method).toEqual('GET');
    expect(usersReq.request.method).toEqual('GET');
    httpTestingController.verify();
  });

  it('should test getRequests - no initiator case', () => {
    let requestsUrl = environment.getValue('requestPath', null, 'WKP-PM-Tool');
    let customerUrl = environment.getValue('customerPath', null, 'WKP-PM-Tool').replace('{customerNdg}', customerNdgExample);

    mockUsers[0] = <User>{};
    expectedRequests[0].initiatorName = '-';
    service.getRequests(customerNdgExample, false).subscribe((requests) => {
      expect(requests).toEqual(expectedRequests);
    });

    const requestsReq = httpTestingController.expectOne(requestsUrl);
    requestsReq.flush({ requests: expectedRequests });
    const usersReq = httpTestingController.expectOne(customerUrl);
    usersReq.flush({ customer: { users: mockUsers } });

    expect(requestsReq.request.method).toEqual('GET');
    expect(usersReq.request.method).toEqual('GET');
    httpTestingController.verify();
  });

  it('should test getRequests - no approver case', () => {
    let requestsUrl = environment.getValue('requestPath', null, 'WKP-PM-Tool');
    let customerUrl = environment.getValue('customerPath', null, 'WKP-PM-Tool').replace('{customerNdg}', customerNdgExample);

    mockUsers[1] = <User>{};
    expectedRequests[0].approverName = '-';

    service.getRequests(customerNdgExample, false).subscribe((requests) => {
      expect(requests).toEqual(expectedRequests);
    });

    const requestsReq = httpTestingController.expectOne(requestsUrl);
    requestsReq.flush({ requests: expectedRequests });
    const usersReq = httpTestingController.expectOne(customerUrl);
    usersReq.flush({ customer: { users: mockUsers } });

    expect(requestsReq.request.method).toEqual('GET');
    expect(usersReq.request.method).toEqual('GET');
    httpTestingController.verify();
  });

  it('should update the status of a request', () => {
    const requestIdExample = '1234';

    service.updateStatus(RequestStatus.REQUEST_APPROVED, 'customerNdgExample', 1234).subscribe();

    const url = environment.getValue('updateRequestPath', null, 'WKP-PM-Tool').replace('{requestId}', requestIdExample);
    const body = { status: RequestStatus.REQUEST_APPROVED };
    const updateRequest = httpTestingController.expectOne((req) => req.method === 'PUT' && req.url === url);

    expect(updateRequest.request.body).withContext('aaaa').toEqual(body);
    expect(updateRequest.request.headers.get('context')).withContext('bbbbb').toEqual('customerNdgExample');

    updateRequest.flush({});

    httpTestingController.verify();
  });
});
