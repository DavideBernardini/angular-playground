import { TestBed } from '@angular/core/testing';

import { SharedFoxesService } from './shared-foxes.service';

describe('SharedFoxesService', () => {
  let service: SharedFoxesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SharedFoxesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
