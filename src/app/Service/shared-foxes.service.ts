import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface ImageFetcher {
  loaded: boolean;
  url: string;
  imageFetch(): void;
}

@Injectable({
  providedIn: 'root'
})
export class SharedFoxesService implements ImageFetcher {

  loaded: boolean;
  url: string;

  constructor(private httpClient: HttpClient) {
    this.loaded = false;
    this.url = '';
  }


  imageFetch(): void {

    this.httpClient
      .get('https://randomfox.ca/floof/')
      .subscribe((response: any) => {
        this.url = response.image;
        this.loaded = true;
      });
  }
}
