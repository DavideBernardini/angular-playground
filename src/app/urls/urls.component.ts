import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-urls',
    templateUrl: './urls.component.html',
    styleUrls: ['./urls.component.scss']
})
export class UrlsComponent implements OnInit {

    @Input() link: any

    constructor() {
        this.link = {
            id: Number,
            url: String
        }
    }

    ngOnInit(): void {
    }

}
