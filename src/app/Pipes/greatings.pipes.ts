import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'greatings'
})
export class GreetingsPipe implements PipeTransform {
    public transform(name: string): string {
      return 'Ciao, ' + name + '!';
    }
}
