import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {

  isLoaded: boolean;
  question:string;
  answer:any;
  correctAnswer:any;
  wrongAnswer:any;

  constructor(private httpClient: HttpClient) {
    this.question = "";
    this.answer = null;
    this.correctAnswer = null;
    this.wrongAnswer = null;
    this.isLoaded = false;
  }

  ngOnInit(): void {
    this.fetchQuestion()
  }

  fetchQuestion(): void {
    this.httpClient
      .get('https://opentdb.com/api.php?amount=10&category=18')
      .subscribe((response: any) => {

        console.log(response['results']);
        this.question = response['results'][0]['question'];
        this.answer = response['results'][0]['correct_answer'];
        this.isLoaded = true;
      });
  }

}
