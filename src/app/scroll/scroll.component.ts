import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-scroll',
  templateUrl: './scroll.component.html',
  styleUrls: ['./scroll.component.scss']
})
export class ScrollComponent implements OnInit {

  @Input() myTitle:string;
  @Input() myText:string;


  constructor() {
    this.myTitle = '';
    this.myText = '';
   }

  ngOnInit(): void {
  }

}
