import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-child-component',
    templateUrl: './child-component.component.html',
    styleUrls: ['./child-component.component.scss']
})
export class ChildComponentComponent implements OnInit {
    newMessage:string;

    constructor() {
      this.newMessage = 'Hello!';
     }

    @Input() newString:string = '';

    @Output() newEvent = new EventEmitter<string>();

    ngOnInit(): void {
    }

    sendMessage(value: string) {

      this.newEvent.emit(value);
    }

}
