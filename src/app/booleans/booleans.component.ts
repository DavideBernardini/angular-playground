import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-booleans',
  templateUrl: './booleans.component.html',
  styleUrls: ['./booleans.component.scss']
})
export class BooleansComponent implements OnInit {

  @Input() seats: boolean[];

  constructor() {
    this.seats = [];
  }


  freeAndTaken(vector: boolean[]) {

    let vect: string[] = [];

    vector.forEach(seat => {
      if (seat)
        vect.push('Libero');
      else
        vect.push('Occupato');
    })

    return vect;
  }

  howManyFree(vector: boolean[]) {
    let counter = 0;

    vector.forEach(seat => {
      if(seat)
        counter++;
    });

    return counter;
  }


  ngOnInit(): void {
  }

}
