import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  greating: string = 'Ciao a tutti!';

  receivedMessage: string = '';

  ngTest: string = '';
  num1: number = 0;
  num2: number = 0;

  color: string = '#fff';

  isOn: boolean = true;

  myTime: number = new Date().getTime();

  myName: string = '';

  myTitle: string = 'lorem ipsum';

  myText: string = 'LOREM IPSUM DOLOR SIT, AMET CONSECTETUR ADIPISICING ELIT. NUMQUAM NOBIS NON, DICTA LAUDANTIUM CUPIDITATE SIT UT CUM MAGNI SAEPE, POSSIMUS NEQUE! AMET SAEPE ADIPISCI EARUM HARUM QUOS CULPA QUO PERSPICIATIS.LOREM IPSUM DOLOR SIT AMET CONSECTETUR ADIPISICING ELIT. CONSEQUATUR NEQUE HARUM AB DOLOREMQUE, TEMPORA REPREHENDERIT. UNDE AB PRAESENTIUM CONSECTETUR QUAERAT SINT MODI NESCIUNT ILLUM EXCEPTURI SOLUTA, VEL ID. ADIPISCI, AD?LOREM IPSUM DOLOR SIT AMET CONSECTETUR, ADIPISICING ELIT. DOLORES MAGNI TEMPORIBUS CULPA ODIO VOLUPTATE DISTINCTIO ERROR DOLORE REICIENDIS. MAXIME IPSUM AT OMNIS IPSA MODI LAUDANTIUM QUAM ALIAS, QUIBUSDAM DOLORE QUAERAT.CUM MAGNI SAEPE, POSSIMUS NEQUE! AMET SAEPE ADIPISCI EARUM HARUM QUOS CULPA QUO PERSPICIATIS.LOREM IPSUM DOLOR SIT AMET CONSECTETUR ADIPISICING ELIT. CONSEQUATUR NEQUE HARUM AB DOLOREMQUE, TEMPORA REPREHENDERIT. UNDE AB PRAESENTIUM CONSECTETUR QUAERAT SINT MODI NESCIUNT ILLUM EXCEPTURI SOLUTA, VEL ID. ADIPISCI, AD?LOREM IPSUM DOLOR SIT AMET CONSECTETUR, ADIPISICING ELIT. DOLORES MAGNI TEMPORIBUS CULPA ODIO VOLUPTATE DISTINCTIO ERROR DOLORE REICIENDIS. MAXIME IPSUM AT OMNIS IPSA MODI LAUDANTIUM QUAM ALIAS, QUIBUSDAM DOLORE QUAERAT.'

  name: string = "Mario";
  lastname: string = "Rossi";
  favFoods: string[] = ['pizza', 'pasta', 'arrosto', 'melanzane'];
  seats: boolean[] = [true, true, false, true, false, false, true];

  myLink: {} = {
    id: 23456,
    url: 'dilrf/heasiuhaeoir/'
  }

  constructor() { }



  getMessage(message: string) {
    this.receivedMessage = message;
  }

  onOff() {
    this.isOn = !this.isOn;
  }





  ngOnInit(): void {
  }

}
