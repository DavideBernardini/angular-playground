import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoxFetcherComponent } from './fox-fetcher.component';

describe('FoxFetcherComponent', () => {
  let component: FoxFetcherComponent;
  let fixture: ComponentFixture<FoxFetcherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoxFetcherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoxFetcherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
