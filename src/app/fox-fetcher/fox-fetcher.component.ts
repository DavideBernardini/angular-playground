import { Component, OnInit } from '@angular/core';
import { SharedFoxesService } from '../Service/shared-foxes.service';

@Component({
  selector: 'app-fox-fetcher',
  templateUrl: './fox-fetcher.component.html',
  styleUrls: ['./fox-fetcher.component.scss']
})
export class FoxFetcherComponent implements OnInit {



  constructor(private sharedFox: SharedFoxesService) { }

  getFox() {
    return this.sharedFox;
  }

  ngOnInit(): void {
    this.sharedFox.imageFetch();
  }



}
