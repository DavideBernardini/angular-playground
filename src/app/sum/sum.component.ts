import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sum',
  templateUrl: './sum.component.html',
  styleUrls: ['./sum.component.scss']
})
export class SumComponent implements OnInit {

  @Input() num1:number;

  @Input() num2:number;

  constructor() {
    this.num1 = 0;
    this.num2 = 0;
  }

  ngOnInit(): void {
  }

}
